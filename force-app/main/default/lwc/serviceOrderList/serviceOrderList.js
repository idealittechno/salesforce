import { LightningElement, api, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { NavigationMixin } from 'lightning/navigation';
import { registerListener, unregisterAllListeners,fireEvent } from 'c/pubsub';
import Orderapi from './orderapi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class ServiceOrderList extends LightningElement {
    @api recordId;
	@track serviceOrderList;
	@track noRecordShow;
	@api isConfirmedPopUpShow = false;
	@api statusOrderUpdateId;
	@api statusUpdateType;
	@wire(CurrentPageReference) pageRef;
	@track titleText = '';
    @track messageText = '';
	@track variant = '';
	@track updateResponseCode = ''; // used to manage error response
	connectedCallback() {
		// Subscription for planSelected pub/sub
        registerListener(
            'service__planSelected',
            this.handlePlanSelected,
            this
        );
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
	}
	// Inside the function fetch the order list once plan will be selected 
    handlePlanSelected(planId) {
        this.recordId = planId;
		let orderUrl = 'https://d1e2y64bbmktck.cloudfront.net/scheduling/servicePlans/'+this.recordId+'/serviceOrders/';
		fetch(orderUrl,
		{
			method : "GET",
			headers : {
				"Content-Type": "application/json"
				
			}
		}).then(function(response) {
			return response.json();
		})
		.then((myJson) =>{
			let order_list = [];
			for(let v of Object.values(myJson)){
				order_list.push(new Orderapi(v.publicId, v.windowStart, v.windowEnd,v.serviceOrderState));
			}
			this.serviceOrderList = order_list;
			if(order_list.length>0){
				this.noRecordShow = false;
			}else{
				this.noRecordShow = true;
			}
		})
		// eslint-disable-next-line no-console
		.catch(e=>console.log(e));
        
	}
	handleOrderSelected(event) {
		// fire the pub/sub once order will be clicked 
		event.planId = this.recordId;
        fireEvent(this.pageRef, 'service__orderSelected', event);
	}
	handleOrderChange(event) {
		this.isConfirmedPopUpShow = true;
		this.statusUpdateType = event.detail.status;
		this.statusOrderUpdateId = event.detail.orderId;
		
	}
	handleConfirmDialogNo(){
		this.isConfirmedPopUpShow = false;
	}
	// Inside the function update status of order 
	handleConfirmDialogYes(){
		this.isConfirmedPopUpShow = false;
		let postData = {
			state: this.statusUpdateType
		};
		let orderUrl = 'https://d1e2y64bbmktck.cloudfront.net/scheduling/servicePlans/'+this.recordId+'/serviceOrders/'+this.statusOrderUpdateId+'/changeState/';
		let thisv = this // this value not used inside the fetch function so used thisv instead of this
		fetch(orderUrl,
		{
			method : "POST",
			body : JSON.stringify(postData),
			headers : {
				"Content-Type": "application/json; charset=utf-8"
				
			}
		}).then(function(response) {
			thisv.updateResponseCode = response.status;
			return response.json();
		
		})
		.then((updateResult) =>{
			if(this.updateResponseCode===200){
				const evt = new ShowToastEvent({
					title: 'Success',
					message: 'Item status updated.',
					variant: 'success'
				});
				this.dispatchEvent(evt);
				fireEvent(this.pageRef, 'service__planSelected', this.recordId);
			}else{
				// show alert at error 
				const evt = new ShowToastEvent({
					title: 'Warning',
					message: updateResult.Error,
					variant: 'warning'
				});
				this.dispatchEvent(evt);
			}
		})
		// eslint-disable-next-line no-console
		.catch(e=>console.log(e));

	}
}