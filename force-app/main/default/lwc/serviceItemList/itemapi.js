export default class Itemapi{
	
	constructor(Id, PlannedDate,WorkCompletedDate, ServiceItemState) {
		this._Id = Id;
		this._PlannedDate = PlannedDate;
		this._WorkCompletedDate = WorkCompletedDate;
		this._ServiceItemState = ServiceItemState;
		
	}
}
