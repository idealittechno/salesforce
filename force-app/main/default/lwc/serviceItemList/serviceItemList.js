import { LightningElement, api, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { NavigationMixin } from 'lightning/navigation';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import Itemapi from './itemapi';

export default class ServiceItemList extends LightningElement {
    @api orderId;
    @api planId;
    @track serviceItemList;
    @wire(CurrentPageReference) pageRef;
    connectedCallback() {
        registerListener(
            'service__orderSelected',
            this.handleItemSelected,
            this
        );
        registerListener(
            'service__planSelected',
            this.handlePlanSelected,
            this
        );
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
    }
    handlePlanSelected(planId){
        // make view blank
        this.serviceItemList = false;
    }
    handleItemSelected(orderId) {
        this.orderId = orderId;
        let itemUrl = 'https://d1e2y64bbmktck.cloudfront.net/scheduling/servicePlans/'+orderId.planId+'/serviceOrders/'+orderId.detail+'/serviceItems/';
        
        fetch(itemUrl,
		{
			method : "GET",
			headers : {
				"Content-Type": "application/json"
				
			}
		}).then(function(response) {
			return response.json();
		})
		.then((myJson) =>{
            let item_list = [];
			for(let v of Object.values(myJson)){
				item_list.push(new Itemapi(v.publicId, v.plannedDate,v.workCompletedDate, v.serviceItemState));
			}
            this.serviceItemList = item_list;
        })
		// eslint-disable-next-line no-console
		.catch(e=>console.log(e));
    }
}