import { createElement } from 'lwc';
import ServicePlanList from 'c/servicePlanList';

const dummyPlanData = [
    {publicId: 5550274892737640, active: true},
    {publicId: 5550274892737641, active: true},
    {publicId: 5550274892737642, active: true},
    {publicId: 5550274892737643, active: true},
];
//global.fetch = jest.fn(() => Promise.resolve( dummyPlanData));
global.fetch = jest.fn(() => Promise.resolve({ json: () => dummyPlanData}))


describe('c-servicePlanList', () => {
    afterEach(() => {
        // The jsdom instance is shared across test cases in a single file so reset the DOM
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }
        jest.clearAllMocks();
    });
    // Helper function to wait until the microtask queue is empty. This is needed for promise
    // timing when calling imperative Apex.
    function flushPromises() {
        // eslint-disable-next-line no-undef
        return new Promise(resolve => setImmediate(resolve));
    }

    it('Plan view load', () => {
        // Create element
        const element = createElement('c-servicePlanList', {
            is: ServicePlanList
        });
        document.body.appendChild(element);
        // Return an immediate flushed promise (after the Apex call) to then
        // wait for any asynchronous DOM updates. Jest will automatically wait
        // for the Promise chain to complete before ending the test and fail
        // the test if the promise ends in the rejected state.
        return flushPromises().then(() => {
            const detailEls = element.shadowRoot.querySelectorAll('p');
            expect(detailEls.length).toBe(4);
        });
    });
});