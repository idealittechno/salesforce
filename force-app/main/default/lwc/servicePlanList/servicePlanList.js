import { LightningElement, wire, track } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';
import Planapi from './planapi';

export default class ServicePlanList extends LightningElement {
    @track servicePlanList;
	@wire(CurrentPageReference) pageRef;
	
    handlePlanSelected(event) {
        fireEvent(this.pageRef, 'service__planSelected', event.detail);
    }
    connectedCallback() {
        fetch('https://d1e2y64bbmktck.cloudfront.net/scheduling/servicePlans/',
		{
			method : "GET",
			headers : {
				"Content-Type": "application/json"
				
			}
		}).then(function(response) {
			return response.json();
		})
		.then((myJson) =>{
			let api_plan_list = [];
			for(let v of Object.values(myJson)){
				api_plan_list.push(new Planapi(v.publicId, v.publicId, v.active));
			}
			this.servicePlanList = api_plan_list; 
		})
		// eslint-disable-next-line no-console
		.catch(e=>console.log(e));

    }
}