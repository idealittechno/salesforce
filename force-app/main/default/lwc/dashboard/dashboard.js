import { LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class Dashboard extends NavigationMixin(LightningElement) {

    navigateToService() {
        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName: 'Service'
            }
        });
    }
}