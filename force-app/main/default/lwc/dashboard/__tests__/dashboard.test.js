import { createElement } from 'lwc';
import Dashboard from 'c/dashboard';

describe('c-dashboard', () => {
    afterEach(() => {
        // The jsdom instance is shared across test cases in a single file so reset the DOM
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }
    });

    it('Displays Dashboard', () => {
        // Create element
        const element = createElement('c-dashboard', {
            is: Dashboard
        });
        document.body.appendChild(element);

        // Verify Displayed Dashboard
        const div = element.shadowRoot.querySelector('p');
        expect(div.textContent).toBe('Performance Assessment Service');
    });
});