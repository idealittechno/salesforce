import { LightningElement,api } from 'lwc';

export default class ServiceItem extends LightningElement {
    @api item;
    @api index;
   
    get position() {
        return this.index + 1;
    }
    get statusFormated() {
        if(this.item._ServiceItemState==='pending'){
            return 'Pending';
        }else{
            return this.item._ServiceItemState;
        }
    }
    
}