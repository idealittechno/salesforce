import { LightningElement,api } from 'lwc';

export default class ServiceOrder extends LightningElement {
    @api order;
    @api index;
    awaitingSetupAction = [
        { value: "pending", label: "Pending",icon:"standard:sales_cadence" },
        { value: "in_progress", label: "In Progress", icon:"custom:custom79" },
        { value: "failed_to_place", label: "Failed to Place", icon:"standard:quip" }
    ];
    pendingAction = [
        { value: "in_progress", label: "In Progress",icon:"custom:custom79" },
        { value: "awaiting_setup", label: "Awaiting Setup", icon:"custom:custom25" }
    ];
    inProgressAction = [
        { value: "work_accepted", label: "Work Accepted", icon:"standard:task2" },
        { value: "awaiting_setup", label: "Awaiting Setup", icon:"custom:custom25" },
        { value: "work_rejected", label: "Work Rejected", icon:"standard:first_non_empty" }
    ];
    workAcceptedAction = [
        { value: "paid", label: "Paid", icon:"standard:thanks" },
        
    ];
    paidAction = [
        { value: "terminal", label: "Terminal", icon:"standard:record_delete" },
        
    ];
    workRejectedAction = [
        { value: "terminal", label: "Terminal", icon:"standard:record_delete" },
        
    ];
    failedToPlaceAction = [
        { value: "terminal", label: "Terminal", icon:"standard:record_delete" },
        
    ];
    noAction = [];
    handleOrderSelected() {
        const selectedEvent = new CustomEvent('selected', {
            detail: this.order._Id,
            
        });
        this.dispatchEvent(selectedEvent);
    }
    actionChange(event){
        // console.log('action start at '+this.order._WindowStart);
        // console.log('event change to   '+event.target.value);
        if(event.currentTarget.dataset.status!==''){
            const selectedAction = new CustomEvent('actionchange', {
                detail:{
                    orderId: this.order._Id,
                    status: event.currentTarget.dataset.status,
                 }
            });
            this.dispatchEvent(selectedAction);
        }   

    }
    
    get position() {
        return this.index + 1;
    }
    get statusFormated() {
        if(this.order._ServiceOrderState==='awaiting_setup'){
            return 'Awaiting Setup';
        }
        else if(this.order._ServiceOrderState==='pending'){
             return 'Pending';
        }else if(this.order._ServiceOrderState==='in_progress'){
             return 'In Progress';
        }
        else if(this.order._ServiceOrderState==='work_accepted'){
            return 'Work Accepted';
       }
       else if(this.order._ServiceOrderState==='paid'){
            return 'Paid';
        }
        else if(this.order._ServiceOrderState==='work_rejected'){
            return 'Work Rejected';
        }
        else if(this.order._ServiceOrderState==='failed_to_place'){
            return 'Failed to Place';
        }
        else if(this.order._ServiceOrderState==='terminal'){
            return 'Terminal';
        }
        else{
            return this.order._ServiceOrderState;
        }
    }
    get getActionDropdown() {
        
        if(this.order._ServiceOrderState==='awaiting_setup'){
            return this.awaitingSetupAction;
        }
        else if(this.order._ServiceOrderState==='pending'){
             return this.pendingAction;
        }else if(this.order._ServiceOrderState==='in_progress'){
             return this.inProgressAction;
        }
        else if(this.order._ServiceOrderState==='work_accepted'){
            return this.workAcceptedAction;
       }
       else if(this.order._ServiceOrderState==='paid'){
            return this.paidAction;
        }
        else if(this.order._ServiceOrderState==='work_rejected'){
            return this.workRejectedAction;
        }
        else if(this.order._ServiceOrderState==='failed_to_place'){
            return this.failedToPlaceAction;
        }
        else{
            return this.noAction;
        }
         
     }
}