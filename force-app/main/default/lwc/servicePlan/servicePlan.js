import { LightningElement,api } from 'lwc';

export default class ServicePlan extends LightningElement {
    @api plan;
    @api index;
    handlePlanSelected() {
        const selectedEvent = new CustomEvent('selected', {
            detail: this.plan._Id
        });
        this.dispatchEvent(selectedEvent);
    }
    get position() {
        return this.index + 1;
    }
}